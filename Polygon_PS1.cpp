#include "Polygon.h"
#include<cassert>

float Polygon::getSignedArea()const noexcept{
    float result = 0.0f;
    
    if(fNumberOfVertices > 2){
        int j = fNumberOfVertices - 1;
        for(size_t i = 0; i < fNumberOfVertices; i++){
            result += (this->getVertex(j).x() - this->getVertex(i).x()) * (this->getVertex(j).y() + this->getVertex(i).y());
            j = i;
        }
    }
    return result / 2.0;
}

Polygon Polygon::transform(const Matrix3x3& aMatrix)const noexcept{
    Polygon Result = *this;
    for(size_t i = 0; i < fNumberOfVertices; i++){
        Result.fVertices[i] = static_cast<Vector2D>(aMatrix * fVertices[i]);
    }
    
    return Result;
}
#define _USE_MATH_DEFINES

#include "Matrix3x3.h"
#include<cassert>
#include<cmath>
#include<string>
#include<iostream>

std::ostream& operator<<(std::ostream &out, const Matrix3x3& aMatrix){
    out << aMatrix.row(0) << std::endl << aMatrix.row(1) << std::endl << aMatrix.row(2) << std::endl;

    return out;
}

Matrix3x3 Matrix3x3::operator*(const Matrix3x3& aOther)const noexcept{
    float nodes[3][3];

    for(int i = 0; i < 3; i++){
        nodes[i][0] = fRows[i].x() * aOther.fRows[0].x() + fRows[i].y() * aOther.fRows[1].x() + fRows[i].w() * aOther.fRows[2].x();
        nodes[i][1] = fRows[i].x() * aOther.fRows[0].y() + fRows[i].y() * aOther.fRows[1].y() + fRows[i].w() * aOther.fRows[2].y();
        nodes[i][2] = fRows[i].w() * aOther.fRows[0].w() + fRows[i].y() * aOther.fRows[1].w() + fRows[i].w() * aOther.fRows[2].x();
    }

    return Matrix3x3(Vector3D(nodes[0][0], nodes[0][1], nodes[0][2]), Vector3D(nodes[1][0], nodes[1][1], nodes[1][2]), Vector3D(nodes[2][0], nodes[2][1], nodes[2][2]));
}

Matrix3x3 Matrix3x3::transpose()const noexcept{
    return Matrix3x3(Vector3D(fRows[0].x(), fRows[1].x(), fRows[2].x()), Vector3D(fRows[0].y(), fRows[1].y(), fRows[2].y()), Vector3D(fRows[0].w(), fRows[1].w(), fRows[2].w()));
}

float Matrix3x3::det() const noexcept{
    float a, b, c;
    a = fRows[0].x() * (fRows[1].y() * fRows[2].w() - fRows[1].w() * fRows[2].y());
    b = fRows[0].y() * (fRows[1].x() * fRows[2].w() - fRows[1].w() * fRows[2].x());
    c = fRows[0].w() * (fRows[1].x() * fRows[2].y() - fRows[1].y() * fRows[2].w());

    return a - b + c;
}

Matrix3x3 Matrix3x3::inverse() const{
    Vector3D row1(fRows[1].y() * fRows[2].w() - fRows[1].w() * fRows[2].y(), fRows[0].w() * fRows[2].y() - fRows[0].y() * fRows[2].w(), fRows[0].y() * fRows[1].w() - fRows[0].w() * fRows[1].y());
    Vector3D row2(fRows[1].w() * fRows[2].x() - fRows[1].x() * fRows[2].w(), fRows[0].x() * fRows[2].w() - fRows[0].w() * fRows[2].x(), fRows[0].w() * fRows[1].x() - fRows[0].x() * fRows[1].w());
    Vector3D row3(fRows[1].x() * fRows[2].y() - fRows[1].y() * fRows[2].x(), fRows[0].y() * fRows[2].x() - fRows[0].x() * fRows[2].y(), fRows[0].x() * fRows[1].y() - fRows[0].y() * fRows[1].x());

    Matrix3x3 m(row1, row2, row3);

    
    return m * (1.0f / this->det());
}

bool Matrix3x3::hasInverse()const noexcept{
    if(this->det() != 0){
        return true;
    }
    return false;
}